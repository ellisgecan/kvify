'use strict';

// console.log(destruct(require(process.argv[2])));
/*
  type is a string of one of the following
    'number'
    'boolean'
    'string'
    'undefined'
    'null'
    'array'
    'object'
*/
return module.exports = {
  destruct: destruct,
  stream: stream,
};

function destruct(obj) {
  
  const path = [];
  const tokens = [];
  stream(obj, function(token) { tokens.push(token); });
  return tokens;
}

function stream(obj, cb) {
  const path = [];
  recurse(obj);
  return;
  
  function recurse(c) {
    cb(createToken(c));
    
    if(typeof c === 'object') { // works with null
      for(var k in c) {
        path.push(k);
        recurse(c[k]);
        path.pop();
      }
    }
  }
  
  function createToken(v) {
    var type = typeof v;
    if(type === 'object') {
      if(v === null) {
        type = 'null'
      } else if(v instanceof Array) {
        type = 'array';
      } // else already object
    }
    
    return {
      k: path.slice(),
      v: v,
      type: type
    };
  }
}

